﻿using System;
using System.Text;

namespace InspectMetricOf
{
	class Program
	{
		static void Main(string[] args)
		{
			var arguments = new CommandLineArguments(args);
			var files = Files.FromCommandlineArguments(arguments);

			files.Inspect();

			Console.WriteLine();
			Console.WriteLine();
			Console.WriteLine("Ready...");
		}
	}
}
