﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace InspectMetricOf
{
	static class ExtensionMethods
	{
		public static bool Invalid(this CommandLineArguments source)
		{
			if (!source.Any())
			{
				Console.WriteLine("Was soll ich denn inspizieren?");
				Console.WriteLine("Rufe bitte die Methode mit einem Verzeichnis auf, in dem die Dateien zum inspizieren liegen.");
				return true;
			}

			var dirctoryToInspect = source.First();

			if (!Directory.Exists(dirctoryToInspect))
			{
				Console.WriteLine("Das angegebene Verzeichnus gibt es gar nicht.");
				Console.WriteLine("Wie soll ich dann korrekt inspizieren, hä?");

				return true;
			}

			return false;
		}

		public static IEnumerable<FileInfo> ToFiles(this CommandLineArguments source)
		{
			var files = new List<FileInfo>();

			if(source.Invalid()) return files;

			

			return files;
		}

		public static string RemoveRedundantCharacters(this string source)
		{
			return Regex.Replace(source, @"[^\;{}]", string.Empty);
		}
	}
}
