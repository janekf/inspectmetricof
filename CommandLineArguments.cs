using System.Collections.Generic;

namespace InspectMetricOf
{
	internal class CommandLineArguments : List<string>
	{
		public CommandLineArguments(string[] originalArguments)
		{
			AddRange(originalArguments);
		}
	}
}