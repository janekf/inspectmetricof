﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Machine.Specifications;

namespace InspectMetricOf
{
	class when_read_files_from_test_folder
	{
		Establish context = () =>
			{
				var args = new string[]{Environment.CurrentDirectory + @"\..\..\ResourceForTests"};
				_args = new CommandLineArguments(args);
			};

		Because of = () => _files = Files.FromCommandlineArguments(_args);

		It should_contains_TestFile = () => _files.Any(file => file.Name == "TestFile.cs").ShouldBeTrue();
		
		static CommandLineArguments _args;
		static Files _files;
	}

	class when_inspect_a_line_with_4_semicolon_and_6_braces
	{
		Establish context = () =>
			{
				_line = "AB{C;de}f;g}h{i;jk}}j;";
			};

		Because of = () => _line = _line.RemoveRedundantCharacters();

		It should_have_a_lenght_of_10_characters = () => _line.Length.ShouldEqual(10);
		It should_contains_now_4_semicolon = () => _line.Count(c => c == ';').ShouldEqual(4);
		It should_contain_now_6_braces = () => _line.Count(c => c == '{' || c=='}').ShouldEqual(6);


		static string _line;
	}
}