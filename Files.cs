using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace InspectMetricOf
{
	class Files : List<FileInfo>
	{
		protected Files()
		{
			
		}

		public static Files FromCommandlineArguments(CommandLineArguments arguments)
		{
			if(arguments.Invalid()) return new Files();

			var directory = arguments.First();
			var filesOfDirectory = Directory.GetFiles(directory, "*.cs",SearchOption.AllDirectories);
			var files = new Files();
			
			files.AddRange(filesOfDirectory.Select(file => new FileInfo(file)));

			return files;
		}

		public void Inspect()
		{
			ForEach(InspectFile);
		}

		private void InspectFile(FileInfo file)
		{
			Console.Write(file.Name + ": ");

			var rowsCount = 0;

			using (var streamOfFile = file.OpenText())
			{
				while (!streamOfFile.EndOfStream)
				{

					var line = streamOfFile.ReadLine();
					line = line.RemoveRedundantCharacters();

					Console.Write(line);
					rowsCount++;

				}

				Console.Write("\tLines: " + rowsCount + "\r\n");
			}
		}
	}
}